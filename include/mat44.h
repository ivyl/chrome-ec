/* Copyright 2015 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

/* Header file for common math functions. */
#ifndef __CROS_EC_MAT_44_H

#define __CROS_EC_MAT_44_H

#include "vec4.h"
#include "util.h"

typedef float mat44_float_t[4][4];
typedef size_t sizev4_t[4];

void mat44_float_decompose_lup(mat44_float_t LU, sizev4_t pivot);

void mat44_float_swap_rows(mat44_float_t A, const size_t i, const size_t j);

void mat44_float_solve(mat44_float_t A, floatv4_t x, const floatv4_t b,
		 const sizev4_t pivot);

#endif  /* __CROS_EC_MAT_44_H */
